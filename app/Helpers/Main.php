<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


function is_admin_view()
{

    if (isset($_SERVER['REQUEST_URI'])) {
        $adminType = explode("?", $_SERVER['REQUEST_URI']);
        $adminType = explode("/", $adminType[0]);

        if (in_array("admin", $adminType)) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

function url_routes()
{

    return '';
}

function sortByPredefinedOrder($array, $sort)
{
    $sort = array_keys($sort);
    $sort = array_flip($sort);
    foreach ($array as $insd) {
        if (!isset($sort[$insd["name"]])) {
            $sort[$insd["name"]] = 10;
        }
    }
    usort($array, function ($a, $b) use ($sort) {
        return $sort[$a['name']] - $sort[$b['name']];
    });
    return $array;
}

function url_custom($path)
{
    return $path;
}


function get_type_sql()
{

    $numbers = [
        'boolean' => "boolean",
        'tinyint' => "tinyInteger",
        'smallint' => "smallInteger",
        'mediumint' => "mediumInteger",
        'integer' => "integer",
        'bigint' => "bigInteger",
        'decimal' => "decimal",
        'float' => "float",
        'double' => "double",
    ];

    $strings = [
        'char' => 'char',
        'varchar' => 'string',
        'uuid' => 'uuid',
        'text' => 'text',
        'mediumtext' => 'mediumtext',
        'longtext' => 'longtext',
    ];

    $datetime = [
        'date' => 'date',
        'datetime' => 'datetime',
        'year' => 'smallInteger',
        'time' => 'time',
        'timetz' => 'timeTz',
        'timestamp' => 'timestamp',
        'timestamptz' => 'timestampTz',
        'datetimetz' => 'dateTimeTz',
    ];

    $lists = [
        'enum' => 'enum',
        'json' => 'json',
        'jsonb' => 'jsonb',
    ];

    $binary = [
        'binary' => 'binary',
//        'blob'=>'binary',
    ];

    $network = [
        'macaddr' => 'macAddress',
    ];

//    $geometry = [
//        'geometrycollection'=>'',
//    ];

    return [
        "numbers" => $numbers,
        "strings" => $strings,
        "datetime" => $datetime,
        "lists" => $lists,
        "binary" => $binary,
        "network" => $network,
//        "geometry" => $geometry,
    ];
}
