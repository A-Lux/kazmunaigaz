<?php


namespace App\Helpers;

use Exception;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Storage;
use Artisan;

class TableClass
{
    protected $tableInfo;
    protected $table;
    protected $columns;
    protected $oldColumns;
    protected $oldColumnsFile;
    protected $doctrineTypeMapping;
    protected $model_list;


    function __construct($table, $columns = [])
    {
        $this->tableInfo = $table;

        $this->doctrineTypeMapping = [];
        foreach (get_type_sql() as $dsql) {
            $this->doctrineTypeMapping = array_merge($this->doctrineTypeMapping, array_keys($dsql));
        }

        if (!file_exists('../app/Model/' . $table["model"])) {
            Artisan::call('make:model ' . $table["model"]);
        }

        $model = app("\App\\Models\\" . $table["model"]);
        $this->table = $model->getTable();


        if (file_exists('../database/json/' . $this->table . '.json')) {
            $olColumFile = file_get_contents('../database/json/' . $this->table . '.json');
            $this->oldColumnsFile = json_decode($olColumFile, true);
        }


        $model_list = file_get_contents("../database/db_list.json");
        $this->model_list = json_decode($model_list, true);


        $this->columns = [];
        foreach ($columns as $colum) {
            if (isset($colum["name"])) {
                if (!isset($colum["type"])) {
                    $colum["type"] = "string";
                }
                if (!isset($colum["nullable"])) {
                    $colum["nullable"] = true;
                }
                array_push($this->columns,
                    $colum
                );
            }
        }
    }

    function save()
    {
        $this->finish();
//        try {
        if (Schema::hasTable($this->table) == true) {
            $this->oldColumns = Schema::getColumnListing($this->table);
            $this->edit();
        } else {
            $this->create();
        }
//        } catch (Exception $e) {
//            echo("error");
//        }
    }

    function edit()
    {
        $blueprintCode = '';

        $noRmCol = [];
        foreach ($this->columns as $indexSaveCol => $column) {
            $null = "";

            if ($column["type"] == true) {
                $null .= "->nullable()";
            }
            if (isset($column["increment"])) {
                $null .= "->autoIncrement()";
            }
            if (!in_array(mb_strtolower($column["name"]), $this->oldColumns)) {
                if (in_array(mb_strtolower($column["nameOld"]), $this->oldColumns) && !in_array(mb_strtolower($column["name"]), $this->oldColumns)) {
                    $blueprintCode .= '$table->renameColumn("' . $column["nameOld"] . '","' . $column["name"] . '");';
                } else {
                    $blueprintCode .= '$table->' . str_replace(array_keys($this->doctrineTypeMapping), array_values($this->doctrineTypeMapping), mb_strtoupper($column["type"])) . "('" . mb_strtolower($column["name"]) . "')" . $null . ";";
                }
            }
            array_push($noRmCol, $column["nameOld"]);
        }

        $removeRow = array_diff($this->oldColumns, $noRmCol);
//        dd($this->columns);
        foreach ($removeRow as $removeName) {
//            $deleteIndex= array_search($removeName, array_column($this->oldColumnsFile, 'nameOld'));
//            dd($this->oldColumns);
            $blueprintCode .= '$table->dropColumn("' . $removeName . '");';
        }

//        dd($blueprintCode);
        Schema::table($this->table, function ($table) use ($blueprintCode) {
            eval($blueprintCode);
        });
        $this->finish();
    }

    function create()
    {
        $blueprintCode = '';
        foreach ($this->columns as $column) {
            $null = "";
            if ($column["type"] == true) {
                $null = "->nullable()";
            }
            if (isset($column["increment"])) {
                $null .= "->autoIncrement()";
            }
            $blueprintCode .= '$table->' . str_replace(array_keys($this->doctrineTypeMapping), array_values($this->doctrineTypeMapping), mb_strtoupper($column["type"])) . "('" . mb_strtolower($column["name"]) . "')" . $null . ";";
        }
        Schema::create($this->table, function (Blueprint $table) use ($blueprintCode) {
            eval($blueprintCode);
        });

        array_push($this->model_list, ["model" => $this->tableInfo["model"], "table" => $this->table, "name" => $this->tableInfo["model"]]);
        $handle = fopen("../database/db_list.json", 'w');
        fwrite($handle, json_encode($this->model_list));
        fclose($handle);


        $this->finish();

    }

    function remove($newarray)
    {
        $path = "../database/json/" . $this->table . ".json";
        Schema::drop($this->table);
        unlink($path);
        $handle = fopen("../database/db_list.json", 'w');
        fwrite($handle, json_encode($newarray));
        fclose($handle);
    }


    function bread($bread)
    {
        $path = "../database/json/bread/" . $this->table . ".json";
        if (file_exists($path)) {
            unlink($path);
        }
        $handle = fopen($path, 'w');
        fwrite($handle, json_encode($bread));
        fclose($handle);
    }

    function finish()
    {
        if (count($this->columns) > 0) {
            $path = "../database/json/" . $this->table . ".json";
            $tableJson = [];
            foreach ($this->columns as $column) {
                array_push($tableJson, $column);
                sort($tableJson);
            }

            foreach ($tableJson as $index => $tba) {
                $tableJson[$index]["nameOld"] = $tba["name"];
            }
            $tableJson = array_reverse($tableJson);
            $handle = fopen($path, 'w');
            fwrite($handle, json_encode($tableJson));
            fclose($handle);
        }
    }
}

