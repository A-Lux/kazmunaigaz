<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;
use Schema;
use NCANodeClient;

class MainController extends Controller
{


    public function main()
    {

//        $insta = DB::table("instagram_posts")->whereRaw('id in(SELECT DISTINCT id FROM instagram_posts GROUP BY instagram_id)')->DISTINCT()->get();
        return view('client.main');
    }

    public function admin_main()
    {
//        $nca = new NCANodeClient('http://127.0.0.1:14579');
//
//        $p12InBase64 = base64_encode(file_get_contents("./nca/Aa1234/RSA256_093a562746e7ddd75fa97feba045991c4d544ee3.p12"));
//        $info_ssh = base64_encode(file_get_contents("./nca/file.txt"));
//
//        $info = $nca->setTime($info_ssh);
//        $files = $info["result"]["tsp"];
//        $text = (base64_decode('$files'));
////        $info = $nca->cmsSign($files, $p12InBase64, "Aa1234");
////        $text = base64_decode($info["cms"]);
//        $fp = fopen("file.cms", "w");
//        fwrite($fp, $text);
//        fclose($fp);



        return view('admin.main');
    }


}
