<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use NCANodeClient;

class MainController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Товары в корзине
    protected $users;


    public function __construct(Request $request)
    {
//        $this->users = auth()->guard('api')->user();
//
//        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
//            $this->middleware(function ($request, $next) {
//                return $this->sendError('вы не авторизованы');
//            });
//        }
    }

    public function ferialfile(Request $request)
    {
        $request = $request->all();

        $file_type = $request["document"]->getClientOriginalExtension();


        $nca = new NCANodeClient('http://127.0.0.1:14579');
        $p12InBase64 = base64_encode((file_get_contents($request["ecp"]->getRealPath())));
        $document = base64_encode(file_get_contents(($request["document"]->getRealPath())));
        $document_cms = $nca->rawSign($document, $p12InBase64, $request["password"]);
        $text = base64_decode($document_cms["result"]["cms"]);

        $file_name = Str::random(4) . "." . $file_type . ".cms";
        $fp = fopen($file_name, "w");
        fwrite($fp, $text);
        fclose($fp);
        return $this->sendResponse($file_name, '');

    }

    public function checkgile(Request $request)
    {
        $request = $request->all();
        $nca = new NCANodeClient('http://127.0.0.1:14579');
        $document = base64_encode(file_get_contents(($request["document"]->getRealPath())));
        $document_cms = $nca->cmsVerify($document);
        return $this->sendResponse($document_cms["result"], '');
    }

    public function typeRowDb()
    {
        $resoebs = get_type_sql();
        foreach ($resoebs as $index => $ksa) {
            $resoebs[$index] = array_values($ksa);
        }
        return $this->sendResponse($resoebs, '');
    }


}
