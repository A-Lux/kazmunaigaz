<?php

namespace App\Http\Controllers\Api\Admin\Database;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use TableClass;
use Storage;
use Str;
use NCANodeClient;

class MainController extends BaseController
{
    protected $users;


    public function __construct(Request $request)
    {
//        $this->users = auth()->guard('api')->user();
//
//        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
//            $this->middleware(function ($request, $next) {
//                return $this->sendError('вы не авторизованы');
//            });
//        }
    }


    public function edit_column_get($modul, $id)
    {

        if (!file_exists("../app/Models/" . $modul . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model = app("\App\\Models\\" . $modul);
        $table = $model->getTable();
        $crow = "add";
        if ($id != "0") {
            $model = $model::find($id);
            if (!is_null($model)) {
                $crow = "edit";
            }
        }


        if (!file_exists("../database/json/bread/" . $table . ".json")) {
            return $this->sendResponse([], 'error');
        }

        $model_list = file_get_contents("../database/json/bread/" . $table . ".json");
        $model_list = json_decode($model_list, true);

        $thead_return = [];
        foreach ($model_list as $name => $thead) {
            if (isset($thead["views"])) {
                if (in_array($crow, $thead["views"])) {
                    if ($thead["type"] == "Image") {
                        $thead["type"] = "file";
                    }
                    if (isset($thead["display"]) ? (str_replace(" ", "", $thead["display"]) != "" ?: false) : false) {
                        array_push($thead_return, ["name" => $name, "placeholder" => $thead["display"], "type" => $thead["type"]]);
                    } else {
                        array_push($thead_return, ["name" => $name, "placeholder" => $name, "type" => $thead["type"]]);
                    }
                }
            }
        }
        return $this->sendResponse($thead_return, '');
    }

    public function browse_column_get($modul)
    {
        if (!file_exists("../app/Models/" . $modul . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model = app("\App\\Models\\" . $modul);
        $table = $model->getTable();

        if (!file_exists("../database/json/bread/" . $table . ".json")) {
            return $this->sendResponse([], 'error');
        }

        $model_list = file_get_contents("../database/json/bread/" . $table . ".json");
        $model_list = json_decode($model_list, true);


        $thead_return = [];
        foreach ($model_list as $name => $thead) {
            if (isset($thead["views"])) {
                if (in_array("browse", $thead["views"])) {
                    if (isset($thead["display"]) ? (str_replace(" ", "", $thead["display"]) != "" ?: false) : false) {
                        $thead_return[$name] = $thead["display"];
                    } else {
                        $thead_return[$name] = $name;

                    }
                }
            }
        }
        return $this->sendResponse($thead_return, '');
    }

    public function database_single_get($model, $id)
    {

        if (!file_exists("../app/Models/" . $model . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model_name = $model;
        $model = app("\App\Models\\$model_name");
        $model = $model->find($id);
        if (is_null($model)) {
            return $this->sendResponse([], 'error');
        }
        return $this->sendResponse($model, '');
    }

    public function browse_delete($model, Request $request)
    {

        if (!file_exists("../app/Models/" . $model . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model_name = $model;
        $model = app("\App\Models\\$model_name");
        $model = $model->find($request["id"]);
        if (is_null($model)) {
            return $this->sendResponse([], 'error');
        }
        $model->delete();
        return $this->sendResponse($model, '');
    }

    public function database_all_get($model)
    {

        if (!file_exists("../app/Models/" . $model . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model_name = $model;
        $model = app("\App\Models\\$model_name");
        $model = $model->get();
        if (is_null($model)) {
            return $this->sendResponse([], 'error');
        }
        return $this->sendResponse($model, '');
    }


    public function database_update(Request $request)
    {
        $files = $request;
        $request = $request->all();

        foreach ($_FILES["save"]["name"] as $nameInput => $fileout) {
            if ($files->hasFile("save." . $nameInput)) {
                $filename = Str::random(6) . "." . $files["save"][$nameInput]->getClientOriginalExtension();
                $files_is = Storage::disk('uploads')->put($filename, file_get_contents($files["save"][$nameInput]->getRealPath()));
                if ($files_is) {
                    $request["save"][$nameInput] = "/storage/uploads/" . $filename;
                } else {
                    unset($request["save"][$nameInput]);
                }
            } else {
                unset($request["save"][$nameInput]);
            }
        }
        if (!file_exists("../app/Models/" . $request["model_name"] . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $id = $request["id"];
        $model_name = $request["model_name"];
        $model = null;
        if ($id == 0) {
            $model = app("\App\Models\\$model_name");
        } else {
            $model = app("\App\Models\\$model_name");
            $model = $model->find($id);
        }

        foreach ($request["save"] as $key => $input) {
            if (is_array($input)) {
                $model->{$key} = json_encode($input, JSON_UNESCAPED_UNICODE);
            } else {
                $model->{$key} = $input;
            }
        }

        if ($request["model_name"] == "Directory") {
            if ($id == 0) {
                $model->name = $files["save"]["file"]->getClientOriginalName();
                $nca = new NCANodeClient('http://127.0.0.1:14579');
                $p12InBase64 = base64_encode((file_get_contents(str_replace("/storage","../storage/app/public",$request["save"]["ecp_file"]))));
                $document = base64_encode(file_get_contents(str_replace("/storage","../storage/app/public", $request["save"]["file"])));
                $document_cms = $nca->rawSign($document, $p12InBase64, $request["save"]["ecp_password"]);
                $text = base64_decode($document_cms["result"]["cms"]);
                $file_cms = Str::random(6) . "." . $files["save"]["file"]->getClientOriginalExtension() . ".cms";
                $files_save = Storage::disk('uploads')->put($file_cms, $text);
                if ($files_save) {
                    $model->ecp_file_out = "/storage/uploads/" . $file_cms;
                }
            }
        }

        $model->save();


        return $this->sendResponse(["id" => $model->id], '');

    }


}
