<?php

namespace App\Http\Controllers\Api\Admin\Сons\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\Admin\Сons\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;

class MainController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Товары в корзине
    protected $users;


    public function __construct(Request $request)
    {
        $this->users = auth()->guard('api')->user();

        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
            $this->middleware(function ($request, $next) {
                return $this->sendError('вы не авторизованы');
            });
        }
    }




}
