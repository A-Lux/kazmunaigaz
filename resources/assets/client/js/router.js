import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter)

import home from './views/home.vue'
import news from './views/news.vue'
import news_inner from './views/news_inner.vue'
import setAzs from './views/setAzs.vue'

const routers = [
    {path: '/client', component: home},
    {path: '/news', component: news},
    {path: '/news_inner', component: news_inner},
    {path: '/set_azs', component: setAzs},
];

export default new VueRouter({
    routes: routers,
    mode: 'history',
});
