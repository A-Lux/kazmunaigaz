require('./bootstrap');
import router from "./router";
window.Vue = require('vue');
import { BootstrapVue } from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.component('index', require('./views/index.vue').default);


const app = new Vue({
    el: '#app',
    router
});

