require('./bootstrap');
import Vue from "vue";
import router from "./router";
import Notifications from 'vue-notification'

Vue.use(Notifications)
Vue.component('layouts', require('./views/layouts/app').default);

const app = new Vue({
    el: '#app',
    router: router
});



